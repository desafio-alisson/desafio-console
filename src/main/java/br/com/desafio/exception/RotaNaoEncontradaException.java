package br.com.desafio.exception;

public class RotaNaoEncontradaException extends RuntimeException {
    public RotaNaoEncontradaException(){
        super("Rota não encontrada");
    }
}
