package br.com.desafio;

import br.com.desafio.repository.model.Aeroporto;
import br.com.desafio.repository.model.Rota;
import br.com.desafio.repository.model.basedados.BaseDados;
import br.com.desafio.service.RotaService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) throws IOException {

        if(args.length != 1){
            throw new RuntimeException("Arquivo csv não foi informado");
        }

        File file = new File(args[0]);
        validaArquivo(file);

        System.out.println("Lendo dados do Arquivo");
        BaseDados.load(file.toPath());
        System.out.println("Leitura Finalizada");
        iniciar();
    }

    private static void iniciar() {
        boolean entradaValida = false;
        String rota = null;
        do {
            System.out.print("Por favor informe uma rota: ");
            Scanner scan = new Scanner(System.in);
            rota = scan.nextLine();
            entradaValida = validaEntrada(rota);

            if(!entradaValida){
                System.out.println("Entrada Inválida!!");
            }

        }while(!entradaValida);
        String split[] = rota.split("-");
        String origem = split[0].trim();
        String destino = split[1].trim();
        RotaService service = new RotaService();
        try {
            Rota melhorRota = service.encontrarMelhorRota(origem, destino);
            imprimirSaida(melhorRota);
        }catch (Exception ex){
            System.out.println("Não foi encontrada rota para origem="+origem+" e "+"destino="+destino);
        }
        iniciar();
    }

    private static void imprimirSaida(Rota melhorRota) {
        System.out.print("Melhor rota: ");
        int count = 0;
        for(Aeroporto v : melhorRota.getAeroportos()){
            count++;
            if(count == melhorRota.getAeroportos().size()){
                DecimalFormat df = new DecimalFormat("#,##0.00");
                String valorFormatado = df.format(melhorRota.getTotal().setScale(2, RoundingMode.HALF_UP).doubleValue());
                System.out.println(v.getNome() + " > R$"+valorFormatado);
            }else{
                System.out.print(v.getNome()+"-");
            }
        }
    }

    private static boolean validaEntrada(String rota) {
        Pattern pattern = Pattern.compile("^([A-Z])+\\s*-\\s*([A-Z])+\\s*$");
        Matcher matcher = pattern.matcher(rota);
        if (!matcher.matches()){
            return false;
        }
        return true;
    }

    private static void validaArquivo(File file) throws IOException {
        if(!file.getName().endsWith("csv")){
            throw new IOException("Somente é aceito arquivo com extenção csv");
        }

        if(!file.exists()){
            throw new FileNotFoundException("Arquivo não encontrado no seguinte caminho="+file.getAbsolutePath());
        }
    }
}
