package br.com.desafio.service;

import br.com.desafio.exception.RotaNaoEncontradaException;
import br.com.desafio.repository.model.Aeroporto;
import br.com.desafio.repository.model.Rota;
import br.com.desafio.repository.RotaRepository;

import java.util.Optional;

public class RotaService {

    public Rota encontrarMelhorRota(String origem, String destino){
        RotaRepository repository = new RotaRepository();
        Aeroporto aeroportoOrigem = new Aeroporto(origem);
        Aeroporto aeroportoDestino = new Aeroporto(destino);
        Optional<Rota> oRota = repository.encontraMelhorRota(aeroportoOrigem, aeroportoDestino);

        if(oRota.isPresent()){
            return oRota.get();
        }
        throw new RotaNaoEncontradaException();
    }
}
