package br.com.desafio.repository;

import br.com.desafio.repository.model.Aeroporto;
import br.com.desafio.repository.model.Rota;
import br.com.desafio.repository.model.basedados.BaseDados;
import br.com.desafio.repository.model.basedados.MenorPreco;

import java.util.Optional;

public class RotaRepository {

    public Optional<Rota> encontraMelhorRota(Aeroporto aeroportoOrigem, Aeroporto aeroportoDestino) {
        MenorPreco menorPreco = new MenorPreco(BaseDados.getBase());
        return menorPreco.encotrarMelhorRota(aeroportoOrigem, aeroportoDestino);
    }
}
