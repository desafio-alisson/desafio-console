package br.com.desafio.repository.model.basedados;

import br.com.desafio.repository.model.Aeroporto;
import br.com.desafio.repository.model.Destino;

import java.io.BufferedReader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseDados {

    private static Path path;
    private static Map<Aeroporto, List<Destino>> base;

    public static void load(Path path){
        BaseDados.path = path;
        base = new HashMap<>();

        try(BufferedReader br = Files.newBufferedReader(path)) {
            String line = br.readLine();
            while (line != null) {
                String split[] = line.split(",");

                if(split.length == 3) {
                    Aeroporto aeroporto = new Aeroporto(split[0].trim());
                    BigDecimal valor = new BigDecimal(split[2].trim());
                    if (!base.containsKey(aeroporto)) {
                        List<Destino> destinos = new ArrayList<>();
                        destinos.add(new Destino(new Aeroporto(split[1].trim()), valor));
                        base.put(aeroporto, destinos);
                    } else {
                        base.get(aeroporto).add(new Destino(new Aeroporto(split[1].trim()), valor));
                    }
                }
                line = br.readLine();
            }
        }catch (Exception e){
            new RuntimeException("Falha ao ler arquivo");
        }
    }

    public static Path getPath() {
        return path;
    }

    public static Map<Aeroporto, List<Destino>> getBase() {
        return base;
    }
}
