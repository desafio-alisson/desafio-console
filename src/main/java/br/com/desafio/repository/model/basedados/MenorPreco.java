package br.com.desafio.repository.model.basedados;

import br.com.desafio.repository.model.Aeroporto;
import br.com.desafio.repository.model.Destino;
import br.com.desafio.repository.model.Rota;

import java.math.BigDecimal;
import java.util.*;

public class MenorPreco {

    private Map<Aeroporto, List<Destino>> map;
    private Rota rotaDTOSelecionada;

    public MenorPreco(Map<Aeroporto, List<Destino>> map){
        this.map = map;
    }

    public Optional<Rota> encotrarMelhorRota(Aeroporto origem, Aeroporto destino){
        List<Destino> arestas = map.get(origem);
        Set<Aeroporto> rotaAerporto = new LinkedHashSet<>();
        rotaAerporto.add(origem);
        encontraDestino(destino, arestas, rotaAerporto, BigDecimal.ZERO);
        return Optional.ofNullable(rotaDTOSelecionada);
    }

    private void encontraDestino(Aeroporto destino, List<Destino> arestas, Set<Aeroporto> rotaAerportos, BigDecimal valor) {
        if(arestas != null){
            for(Destino a : arestas){
                if(!rotaAerportos.contains(a.getAeroporto())) {
                    if (a.getAeroporto().equals(destino)) {
                        Rota tempRota = new Rota(new LinkedHashSet<>(rotaAerportos), valor);
                        tempRota.addAeroporto(a.getAeroporto());
                        tempRota.addTotal(a.getValor());
                        verificaMenorPreco(tempRota);
                    } else {
                        Set<Aeroporto> tempRotaAeroportos = new LinkedHashSet<>(rotaAerportos);
                        tempRotaAeroportos.add(a.getAeroporto());
                        encontraDestino(destino, map.get(a.getAeroporto()), tempRotaAeroportos, valor.add(a.getValor()));
                    }
                }
            }
        }
    }

    private void verificaMenorPreco(Rota tempRota) {
        if(rotaDTOSelecionada == null){
            rotaDTOSelecionada = tempRota;
        }else{
            if(rotaDTOSelecionada.getTotal().compareTo(tempRota.getTotal()) == 0) {
                // Se os valores forem iguais pega a rota que tem menos escala
                if(rotaDTOSelecionada.getAeroportos().size() > tempRota.getAeroportos().size()){
                    rotaDTOSelecionada = tempRota;
                }
            }else if(rotaDTOSelecionada.getTotal().compareTo(tempRota.getTotal()) > 0){
                rotaDTOSelecionada = tempRota;
            }
        }
    }

}
