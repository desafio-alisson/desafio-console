package br.com.desafio.repository.model;

import java.math.BigDecimal;

public class Destino {
    private Aeroporto aeroporto;
    private BigDecimal valor;

    public Destino(Aeroporto aeroporto, BigDecimal valor){
        this.aeroporto = aeroporto;
        this.valor = valor;
    }

    public BigDecimal getValor(){
        return valor;
    }

    public Aeroporto getAeroporto() {
        return aeroporto;
    }
}
