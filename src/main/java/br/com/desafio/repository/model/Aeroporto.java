package br.com.desafio.repository.model;

import java.util.Objects;

public class Aeroporto {

    private String nome;


    public String getNome() {
        return nome;
    }

    public Aeroporto(String nome){
        this.nome = nome;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aeroporto vertice = (Aeroporto) o;
        return Objects.equals(nome, vertice.nome);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nome);
    }

}
