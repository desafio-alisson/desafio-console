package br.com.desafio.repository.model;

import java.math.BigDecimal;
import java.util.LinkedHashSet;
import java.util.Set;

public class Rota {
    private Set<Aeroporto> aeroportos;
    private BigDecimal total;

    public Rota(){
        this(new LinkedHashSet<>(), BigDecimal.ZERO);
    }

    public Rota(Set<Aeroporto> aeroportos, BigDecimal total){
        this.aeroportos = aeroportos;
        this.total = total;
    }

    public void addTotal(BigDecimal valor){
        total = total.add(valor);
    }

    public BigDecimal getTotal(){
        return total;
    }

    public Set<Aeroporto> getAeroportos(){
        return aeroportos;
    }

    public void addAeroporto(Aeroporto aeroporto){
        aeroportos.add(aeroporto);
    }
}
